package com.example.kalkulator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TEXT_VIEW = "text_view";
    private static final String TEXT_VIEW_UP = "text_view_up";
    private static final String PROCESS = "process";
    private static final String NUMBER = "number1";
    private static final String NUMBERSEC = "number2";

    Button clear;
    Button plus;
    Button minus;
    Button times;
    Button divide;
    Button equals;
    Button procentage;
    Button plusOrMinus;
    Button point;
    TextView input;
    TextView output;

    Button zero;
    Button one;
    Button two;
    Button three;
    Button four;
    Button five;
    Button six;
    Button seven;
    Button eight;
    Button nine;
    String process;

    Button pow2;
    Button pow3;
    Button factorial;
    Button log;
    Button piersq;

    private String operation = "null";
    private String history;
    double answer = 0, number1 = 0, number2 = 0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.mylayout);


        clear = findViewById(R.id.ac);
        plus = findViewById(R.id.add);
        minus = findViewById(R.id.substraction);
        times = findViewById(R.id.multiply);
        divide = findViewById(R.id.divide);
        equals = findViewById(R.id.equals);
        procentage = findViewById(R.id.procentage);
        plusOrMinus = findViewById(R.id.plusorminus);
        point = findViewById(R.id.dot);
        input = findViewById(R.id.screenIn);
        output = findViewById(R.id.screenOut);

        zero = findViewById(R.id.number0);
        one = findViewById(R.id.number1);
        two = findViewById(R.id.number2);
        three = findViewById(R.id.number3);
        four = findViewById(R.id.number4);
        five = findViewById(R.id.number5);
        six = findViewById(R.id.number6);
        seven = findViewById(R.id.number7);
        eight = findViewById(R.id.number8);
        nine = findViewById(R.id.number9);

        pow2 = findViewById(R.id.power2);
        pow3 = findViewById(R.id.power3);
        log = findViewById(R.id.log);
        factorial = findViewById(R.id.factorial);
        piersq = findViewById(R.id.sqrt);


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText("");
                output.setText("");
            }
        });
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "0");
            }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "1");
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "2");
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "3");
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "4");
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "5");
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "6");
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "7");
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "8");
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                input.setText(process + "9");
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                output.setText(process);                          
                input.setText("");
                operation = "+";
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                output.setText(process);
                input.setText("");
                operation = "-";
            }
        });

        times.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                output.setText(process);
                input.setText("");
                operation = "*";
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process = input.getText().toString();
                output.setText(process);
                input.setText("");
                operation = "/";
            }
        });

        point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //process = input.getText().toString();
                //input.setText(process+",");
                //process = process + ".";
                //number1 =Double.parseDouble(process) ;
                String temp = input.getText() + ".";
                input.setText(temp);
            }
        });

        procentage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    process = input.getText().toString();
                    input.setText(process);
                    //operation = "p";
                    input.setText(String.valueOf(Double.parseDouble((String) input.getText()) / 100));
                    System.out.println("Button pushed % " + operation + " " + process);

                }catch (NumberFormatException e){
                    input.setText("No values");
                }
            }
        });

        plusOrMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double temp;
                    process = input.getText().toString();
                    if (Double.parseDouble(process) == 0.0 || Double.parseDouble(process) == 0) {
                        temp = 0.0;
                    } else {
                        temp = Double.parseDouble(process) * -1;
                    }
                    input.setText(String.valueOf(temp));
                }catch (NumberFormatException e){
                    input.setText("No value");
                }
            }
        });

        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (operation == "null"){
                        input.setText("No values");
                    }
                    else {
                        calculate();
                        history = number2 + " " + operation + " " + number1;
                        output.setText(history);
                    }
                }
                catch (NumberFormatException e){
                    input.setText("No values");
                }


            }
        });
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            pow2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        process = input.getText().toString();
                        double tmp = Math.pow(Double.parseDouble(process), 2);
                        output.setText(input.getText() + "^2");
                        input.setText(String.valueOf(tmp));
                    }
                    catch(NumberFormatException e){
                        input.setText("No value");
                    }
                    }
            });

            pow3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        process = input.getText().toString();
                        double tmp = Math.pow(Double.parseDouble(process), 3);
                        output.setText(input.getText() + "^3");
                        input.setText(String.valueOf(tmp));
                    }
                    catch(NumberFormatException e){
                        input.setText("No value");
                    }
                }
            });

            log.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        process = input.getText().toString();
                        double tmp = Math.log10(Double.parseDouble(process));
                        output.setText("log(" + input.getText() + ")");
                        input.setText(String.valueOf(tmp));
                    }
                    catch(NumberFormatException e){
                        input.setText("No value");
                    }
                }
            });

            piersq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        process = input.getText().toString();
                        double tmp = Math.sqrt(Double.parseDouble(process));
                        output.setText("sqrt(" + input.getText() + ")");
                        input.setText(String.valueOf(tmp));
                    }
                    catch (NumberFormatException e){
                        input.setText("No value");
                    }
                }
            });

            factorial.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(input.getText() == null)
                        input.setText("No value");
                    else {
                        process = input.getText().toString();
                        try {
                            int tmp = (int) Math.floor(Double.parseDouble(process));
                            if (tmp >= 13)
                                input.setText("0");
                            else {
                                input.setText(String.valueOf(calfactorial(tmp)));
                                output.setText(process + "!");
                            }
                        } catch (NumberFormatException e) {
                            input.setText("Not integer value");
                        }

                    }
                }
            });

        }




    }


    private void calculate(){
        switch(operation){
            case "+":
                number1 = Double.parseDouble((String) input.getText());
                number2 = Double.parseDouble((String) output.getText());
                answer = number1 + number2;
                input.setText(String.valueOf(answer));
                break;
            case "-":
                number1 = Double.parseDouble((String) input.getText());
                number2 = Double.parseDouble((String) output.getText());
                answer = number2 - number1;
                input.setText(String.valueOf(answer));
                break;
            case "/":
                number1 = Double.parseDouble((String) input.getText());
                number2 = Double.parseDouble((String) output.getText());
                answer = number2 / number1;
                if(Double.parseDouble((String) input.getText()) == 0)
                    input.setText("err");
                else
                    input.setText(String.valueOf(answer));
                break;
            case "*":
                number1 = Double.parseDouble((String) input.getText());
                number2 = Double.parseDouble((String) output.getText());
                answer = number1 * number2;
                input.setText(String.valueOf(answer));
                break;
            //case "p":
                //number1 = Double.parseDouble(output.getText().toString());
                //answer = (number1 / 100);
                //input.setText(String.valueOf(answer));

               // break;


        }

    }


    private int calfactorial(int number) {
        int factor = 1;
        for(int i = 1; i <= number; i++ ){
            factor *= i;
        }
        return factor;
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TEXT_VIEW, (String) input.getText());
        outState.putString(TEXT_VIEW_UP, (String) output.getText());
        outState.putString(PROCESS, process);
        outState.putDouble(NUMBER, number1);
        outState.putDouble(NUMBERSEC, number2);
        outState.putString("operation",operation);
    }

    protected  void onRestoreInstanceState(Bundle savedInstanceState){
        String textInputSaved = savedInstanceState.getString(TEXT_VIEW);
        String textOutputSaved = savedInstanceState.getString(TEXT_VIEW_UP);
        process = savedInstanceState.getString(PROCESS);
        number1 = savedInstanceState.getDouble(NUMBER);
        number2 = savedInstanceState.getDouble(NUMBERSEC);
        operation = savedInstanceState.getString("operation");

        input.setText(textInputSaved);
        output.setText(textOutputSaved);
        super.onRestoreInstanceState(savedInstanceState);
    }

}
